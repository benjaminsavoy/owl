import threading
import logging
from time import sleep
from sched import scheduler
from datetime import datetime

import requests
from dateutil.parser import parse

import config

_logger = logging.getLogger('arrivals')

class ArrivalsBoard(object):
    def __init__(self, description):
        self.description = description

    def send_arrival(self, arrival_text):
        # TODO: Schedule this to run at 10 seconds before, not the same minute.
        _logger.info('{} - Announcement: {}'.format(self.description, arrival_text))

    def send_following_arrivals(self, arrival_texts):
        _logger.info('{} - Arrival Board:'.format(self.description))
        for text in arrival_texts:
            _logger.info('{} - {}'.format(self.description, text))

class Arrival(object):
    def __init__(self, service_id, destination, date):
        self.service_id = service_id
        self.destination = destination
        self.date = date

    # This is the only method required to make something sortable.
    def __lt__(self, other):
        return self.seconds < other.seconds

    @property
    def seconds(self):
        # We don't know if the timezone always is UTC, so we make sure
        # to compare the same timzeone to get the correct time difference.
        date_diff = self.date - datetime.now(self.date.tzinfo)
        return date_diff.days * 60 * 60 * 24 + date_diff.seconds

    @property
    def minutes(self):
        return self.seconds // 60

    def __str__(self):
        if self.minutes == 0:
            minutes_text = 'inbound'
        else:
            minutes_text = '{} min'.format(self.minutes)
        return '{}: {}'.format(self.destination, minutes_text)

class ArrivalsChecker(object):
    def __init__(self, line, stop_point, board):
        # TODO: Check line and stop_point validity here.
        self.line = line
        self.stop_point = stop_point
        self.board = board
        self.future_arrivals = {}
        # TODO: Add a mutex on this?
        self.running = False
        self.thread = None

    def start(self):
        assert not self.running
        _logger.info(('Start updating arrivals for {} line, stop point: {}'
                     ).format(self.line, self.stop_point))
        self.running = True
        self.thread = threading.Thread(target=self.run, args=())
        self.thread.start()

    def run(self):
        while self.running:
            self.update()
            # NB: This may cause us to wait the update interval even though
            # the thread is set to stop. Consider other alternatives.
            sleep(config.UPDATE_INTERVAL)
        # Clean up
        self.future_arrivals = {}

    def stop(self):
        assert self.running
        _logger.info(('Stop updating arrivals for {} line, stop point: {}'
                     ).format(self.line, self.stop_point))
        self.running = False
        self.thread = []

    def check_announcements(self, arrivals):
        current_arrivals = set(self.future_arrivals.keys())
        for arrival in arrivals:
            # If it is a service we did not know of before, add it to the
            # possible current_arrivals to be announced, in case it is due
            # this same minute
            if arrival.service_id not in current_arrivals:
                if arrival.minutes <= 0:
                    current_arrivals.add(arrival.service_id)
                self.future_arrivals[arrival.service_id] = arrival
            # If the service isn't due this minute, we can remove it from
            # the current_arrivals set, but make sure it is up to date in the
            # dict of future_arrivals
            elif arrival.minutes >= 1:
                current_arrivals.remove(arrival.service_id)
                self.future_arrivals[arrival.service_id] = arrival

        # Finally, we announce anything that is due this same minute.
        for service_id in current_arrivals:
            arrival = self.future_arrivals.pop(service_id)
            if arrival.minutes <= 0:
                self.board.send_arrival(arrival)
            else:
                _logger.warning(('An upcoming arrival disappeared: {}'
                                ).format(arrival))

    def update(self):
        url = ('https://api.tfl.gov.uk/Line/{line}'
               '/Arrivals/{stop_point}?direction=inbound'
               ).format(line=self.line, stop_point=self.stop_point)
        result = requests.get(url).json()
        arrivals = [Arrival(service['id'],
                            service['towards'],
                            parse(service['expectedArrival']))
                    for service in result]
        self.check_announcements(arrivals)
        top_3_arrival_texts = map(str, sorted(arrivals)[:3])
        self.board.send_following_arrivals(top_3_arrival_texts)

if __name__ == '__main__':
    arrivals = ArrivalsChecker('Bakerloo', '940GZZLUWLO', ArrivalsBoard('Bakerloo Southbound'))
    arrivals.start()
    sleep(0.3)
    arrivals2 = ArrivalsChecker('Northern', '940GZZLUCTN', ArrivalsBoard('Northern Northbound'))
    arrivals2.start()
    sleep(0.3)
    arrivals3 = ArrivalsChecker('Northern', '940GZZLUGDG', ArrivalsBoard('Northern Southbound'))
    arrivals3.start()
