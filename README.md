# TFL Arrivals Board and Announcements

The tfl.arrivals module can be used to update an arrival board and announce
arrivals the same minute as they are expected to arrive on the platform.

## Configuration

Currently the configuration is hardcoded in the tfl/config.py file, it
configures a logger and an interval at which the arrivals board is refreshed
and the announcements are sent. One better way to do it would be to
pass a configuration to an ArrivalsChecker constructor.

## Try it out!

    virtualenv env -p python3.6
    . env/bin/activate
    pip install -r requirements.txt
    python tfl/arrivals.py

This will give you a simple example of what would happen at the Waterloo
station Southbound Bakerloo line platform.

## Usage

    import tfl.arrivals as arrivals

    # ArrivalsBoard must define send_arrival, and send_following_arrivals
    # as per the specification.
    arrivals = arrivals.ArrivalsChecker(LINE, STOP_POINT, ArrivalsBoard())
    arrivals.start()

    # This can also be stopped, if needed:
    arrivals.stop()


## Threading:

The code in its current form uses Python threads to update the board
every minute. I am not convinced the implementation is fully thread-safe,
so having a mechanism to just spin up a process for each board to be updated
may be an alternative. Other than that, the current implementation might
be thread-safe if a mutex were to surround the ArrivalsChecker.running
property. Other than that, asyncio might be a more elegant solution,
but I do not know enough about it to use it properly yet.

## Other Concerns:

Currently the arrivals are announced the same minute as they are expected.
This may be done better by scheduling them to be announced about 10 seconds
before they are expected.

The code doesn't check whether the line or stop point are valid,
an improvement would be to do that when it is created.

## Testing:

One can spin up many mock arrival boards to test that the program is stable,
and announces things when expected, but that is only a very crude form of
testing. One could do some automated testing by mocking up the api calls
with some historical data as well as the datetime.now() function to match,
thus allowing us to test through many days of data in no time.
